const express = require('express');
const https = require('https');
const cors = require('cors');
const fs = require('fs');
const app = express();

app.post('/no-cors', function(req, res) {
  res.json({
    status: 'success'
  });
});

const corsOptions = {
  // origin: 'https://example.com' // test different origin allowed
  origin: 'https://127.0.0.1:8081'
};

const sslOptions = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

// enable pre-flight request
app.options('/api/asiamall/asiamall_alphapod/staging', cors(corsOptions));
app.post('/api/asiamall/asiamall_alphapod/staging', cors(corsOptions), function(
  req,
  res
) {
  res.json({
    status: 'success'
  });
});

app.options('/api/asiamall/asiamall_alphapod/live', function(req, res) {
  res.set({
    'Access-Control-Allow-Origin': corsOptions.origin,
    'Access-Control-Allow-Headers': 'content-type',
    'Access-Control-Allow-Methods': 'GET,PUT,POST'
  });
  res.sendStatus(204);
});
app.post('/api/asiamall/asiamall_alphapod/live', function(req, res) {
  res.set({
    'Access-Control-Allow-Origin': corsOptions.origin
  });
  res.json({
    status: 'success'
  });
});

https.createServer(sslOptions, app).listen(8443);
