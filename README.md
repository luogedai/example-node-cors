This is an example project of CORS-enabled api with expressjs.

# Install
```
nvm use
npm install
```

# Run
```
npm run api-server
npm run http-server
open https://127.0.0.1:8081
```

